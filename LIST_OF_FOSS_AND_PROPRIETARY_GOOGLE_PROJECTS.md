# List of FOSS and proprietary Google projects

## FOSS

### Operating Systems

1. [Android](https://android.googlesource.com/) ([Apache License 2.0](https://source.android.com/setup/start/licenses))
2. [Fuchsia](https://fuchsia.googlesource.com/) ([Custom MIT-Style License](https://fuchsia.googlesource.com/fuchsia/+/master/LICENSE))
   - [Zircon Kernel](https://fuchsia.googlesource.com/fuchsia/+/master/zircon) ([Custom MIT-Style License](https://fuchsia.googlesource.com/fuchsia/+/master/zircon/LICENSE))
3. [ChromiumOS](https://chromium.googlesource.com/chromiumos/) ([CCPL (?)](https://www.chromium.org/chromium-os/licenses))

### Fonts

1. [Noto](https://github.com/googlefonts/noto-source) ([Apache License 2.0](https://github.com/googlefonts/noto-source/blob/master/LICENSE))
2. [Roboto](https://github.com/googlefonts/roboto) ([Apache License 2.0](https://github.com/googlefonts/roboto/blob/master/LICENSE))

### Developement Tools

1. [Dart](https://github.com/dart-lang/language) ([3-Clause BSD License](https://github.com/dart-lang/language/blob/master/LICENSE))

## Proprietary

### Operating Systems

1. [ChromeOS](https://google.com/intl/en_uk/chromebook/chrome-os/)
2. [Google Auto](https://www.android.com/intl/auto/)
3. [WearOS](https://wearos.google.com)
4. [Android TV](https://android.com/tv/)

### Communication

1. [Gmail](https://gmail.com)
2. [Google Messages](https://messages.google.com)
3. [Google Meet](https://meet.google.com/)

### Advertising Services

1. [Google Ads](https://ads.google.com)

### Entertainment

1. [Youtube](https://www.youtube.com/)
   - [Youtube Music](https://music.youtube.com)

### Search tools

1. [Google](https://google.com)
   - [Google Images](https://images.google.com)
   - [Google Books](https://books.google.com)
   - [Google Patents](https://patents.google.com)
   - [Google Scholar](https://scholar.google.com)