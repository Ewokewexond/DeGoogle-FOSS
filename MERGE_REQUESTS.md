# Merge Requests

We have sent various Merge Requests to repositories on GitHub encouraging the developer(s) to stop using proprietary surveillance-ware, e.g. Google Analytics.

## Legend:

- [x] [URL] = ***Merged***
- [ ] [URL] = ***Active***
- [ ] ~~[URL]~~ = ***Rejected***

## List of merge requests

1. [ ] https://github.com/wowthemesnet/jekyll-theme-memoirs/pull/6
2. [ ] ~~https://github.com/b2a3e8/jekyll-theme-console/pull/18~~
3. [ ] https://github.com/barryclark/jekyll-now/pull/1651
4. [ ] ~~https://github.com/erlang/erlang.github.io/pull/15~~
5. [ ] https://github.com/webpack/webpack.js.org/pull/3759
6. [ ] ~~https://github.com/lodash/lodash.com/pull/226~~
7. [x] https://github.com/linuxmint/linuxmint.github.io/pull/4
8. [ ] https://github.com/alainpham/alainpham.github.io/pull/3
9. [x] https://github.com/wu-kan/wu-kan.github.io/pull/9
10. [ ] https://github.com/murraco/jekyll-theme-minimal-resume/pull/16
11. [x] https://github.com/sujaykundu777/devlopr-jekyll/pull/39
12. [x] https://github.com/christianezeani/panthera-jekyll/pull/1
13. [ ] https://github.com/thelehhman/plainwhite-jekyll/pull/58
14. [ ] https://github.com/chrisrhymes/bulma-clean-theme/pull/46
15. [x] https://github.com/link9596/hydrogen/pull/7
16. [ ] https://github.com/xukimseven/HardCandy-Jekyll/pull/22
17. [ ] https://github.com/samesies/barber-jekyll/pull/36
18. [ ] https://github.com/chrisbobbe/jekyll-theme-prologue/pull/74
19. [ ] https://github.com/tocttou/hacker-blog/pull/13
20. [ ] https://github.com/jameshamann/jekyll-material-theme/pull/42
21. [ ] https://github.com/samanyougarg/hanuman/pull/31
22. [ ] https://github.com/jekyller/jasper2/pull/108
23. [x] https://github.com/aspirethemes/type/pull/16
24. [x] https://github.com/openSUSE/searchPage/pull/28
25. [ ] https://github.com/openSUSE/search-o-o/pull/10
25. [ ] https://github.com/dtao/lazy.js/pull/230
26. [ ] https://github.com/LineageOS/www/pull/18
27. [x] https://github.com/xmonad/xmonad-web/pull/14
28. [ ] ~~https://github.com/tootsuite/documentation/pull/781~~
29. [ ] https://github.com/libscore/site/pull/28
30. [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/21998
31. [ ] https://github.com/os-js/manual.os-js.org/pull/28
32. [ ] https://github.com/yeoman/yeoman.io/pull/810
33. [ ] https://github.com/majodev/google-webfonts-helper/pull/110
34. [ ] https://github.com/desandro/masonry-docs/pull/66
35. [ ] https://github.com/Shopify/draggable/pull/398
36. [ ] https://github.com/twbs/ratchet/pull/906
37. [ ] https://github.com/ionic-team/ionic-site/pull/1546
38. [ ] https://github.com/HubSpot/pace/pull/498
39. [X] https://github.com/opal/opal.github.io/pull/61
40. [ ] ~~https://github.com/jashkenas/coffeescript/pull/5327~~
41. [ ] https://github.com/nostalgic-css/NES.css/pull/430
42. [ ] ~~https://github.com/ipfs/blog/pull/447~~
43. [ ] ~~https://github.com/FromEndWorld/LOFFER/pull/24#event-3427427280~~
44. [X] https://github.com/mono/md-website/pull/110
45. [ ] https://github.com/ipfs/blog/issues/448