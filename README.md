# `DeGoogle-FOSS`, by [ReverseEagle](https://reverseeagle.org)

*Taking the G out of FOSS.*

This is a repository for the DeGoogle-FOSS project by the ReverseEagle team. 

You will find information about what FOSS projects use proprietary Google components; what Google projects are FOSS; which are proprietary; issues and merge requests we have submitted to FOSS developers (see below).

Have you found a FOSS project that uses proprietary Google software? [**File an issue**](https://codeberg.org/ReverseEagle/DeGoogle-FOSS/issues/new), or email one of the ReverseEagle admins!

---

## Google's proprietary components in FOSS

[There is a list of FOSS projects that use Google here!](PROJECTS_USING_GOOGLE.md)

## Google's proprietary software

If you're looking for a list of proprietary and FOSS Google components, [refer to this list.](LIST_OF_FOSS_AND_PROPRIETARY_GOOGLE_PROJECTS.md)

This list may be incomplete. To add something, you can send us a Merge Request.

## What we're doing

Us, and our community, have endlessly been submitting merge requests and issues to FOSS maintainers, highlighting the importance of removing Google's proprietary software.

- [There's a list of issues here.](ISSUES.md)
- [Another list containing our merge requests is here.](MERGE_REQUESTS.md)

If you want to help out, feel free to submit issues to the repositories of FOSS projects that use Google's software.

If you're a developer, you could even send a merge request, removing Google software from said project.

## Get in touch!

We're available on quite a few platforms, and we're quite friendly (or so we've been told).

- [Homepage](https://reverseeagle.org/)
- [Lemmy: `/c/reverseeagle`](https://dev.lemmy.ml/c/reverseeagle)
- [[matrix]: `#reverseeagle:privacytools.io`](https://matrix.to/#/!TDiDZaaFzkUSHRWcxQ:chat.endl.site?via=privacytools.io&via=matrix.org&via=chat.endl.site)