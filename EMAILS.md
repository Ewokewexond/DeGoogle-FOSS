# Emails


## 1. http://unpaywall.org/

- Sent to: team@ourresearch.org
- Author: theevilskely@tutanota.com

## Please remove Google Analytics

>
> Hi and thanks for getting in touch! 
> 
> You make great points. Unfortunately we are super behind right now and it's a tough time to make changes to things that aren't absolutely necessary. We'll keep this suggestion in mind for later though. I really appreciate your taking the time to write such a clear and well-cited email.
> 
> Best,
> 
> Jason
>> 
>> Greetings Unpaywall,
>> 
>> I am very pleased of your service (http://unpaywall.org/), but I have found an issue that it is using Google Analytics, which is why I am emailing you.
>> 
>> Google Analytics is proprietary, and the source code of your services are open source in which I am very pleased, but do not completely support the service due to the use of Google Analytics. To expand, there have had many concerns of Google's privacy practices, as well as many criticism of Google. I am not going to go through the issues since the Wikipedia links I have provided summarize the issues very well.
>> 
>> Thankfully, there are FOSS and ethical alternatives to Google Analytics that exist. There is a service from Swiso called switching.software. They compile lists of ethical alternatives to anti-privacy services. Thankfully, they have a list for Google Analytics, which you can take a look here: https://switching.software/replace/google-analytics.
>> 
>> A small FYI, the ElementaryOS team also switched from Google Analytics to a FOSS and ethical alternative in their website: https://mobile.twitter.com/CassidyJames/status/1275208242793046018. Hope you do it as well.
>> 
>> Thank you.
>> 
>> --
>>
>> Sent with Tutanota:
>> https://tutanota.com

## 2. https://privacyshield.gov

- Sent to: (webform, email unknown)
- Author: resynth1943@tutanota.com

> To whom it may concern,
>
> After taking a brief look at your site, I believe you have made a string of grave errors.
>
> This site, privacyshield.gov, uses a shocking variety of developer tooling that is actually against freedom, privacy and security.
>
> For example, your website uses Google Analytics, a tool by Google which monitors web pages. This also collects a virtual browsing history of said user, exposing them to this monstrous advertising company that's collecting everything it can on everyone.
>
> Then you use CDNJS: a JavaScript CDN proxied behind Cloudflare, of all things, to serve JavaScript code which is crucial to the functionality of your site. As we have seen today, Cloudflare has centralised a large majority of the 'open internet', and now renders it useless with insanely long outages.
>
> Just looking at the list now, and I can see: Google Analytics; CDNJS; Google Tag Manager; Google reCAPTCHA; NETDNA, which seems to be some sort of machine learning framework. There's a lot more; that's just scratching the surface.
>
> Personally, I think your current stance on these services is extremely misleading: when a source claims to be a bastion of privacy, even including it in its domain, it goes without saying that users would expect one thing: privacy.
