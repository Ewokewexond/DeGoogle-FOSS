# Projects using Google

This is a list of FOSS projects relying on proprietary Google components.

## Legend

### Merged/Accepted

- [x] [NAME] uses Google **[SERVICE]** [see [pull#X] = ***Merged***
- [x] [NAME] uses Google **[SERVICE]** [see [issue#X] = ***Accepted***

### Waiting for answer

- [ ] [NAME] uses Google **[SERVICE]** [see [pull/issue#X]

### Rejected

- [ ] ~~[NAME] uses Google **[SERVICE]** [see [pull/issue#X]~~

### Merge Request / Issue not submitted so far

- [ ] [NAME] uses Google **[SERVICE]**

#### Other

*Italicized text represents a subject of lower priority.*

(Fonts and CDNs belong in this category.)

## List

### Operating Systems (OS)

1. [x] [Linux Mint's search page](https://www.linuxmint.com/start/) uses Google **Search**; Google **Analytics** [see [pull#4](https://github.com/linuxmint/linuxmint.github.io/pull/4)]
2. [X] [openSUSE's search page](https://github.com/openSUSE/searchPage) uses Google **Search**
3. [ ] [openSUSE's new search page](https://search.opensuse.org/#) uses Google **Search**
4. [ ] [openSUSE's website](https://www.opensuse.org) uses Google *Fonts*
5. [ ] [FreeBSD](https://www.freebsd.org/) encourages use of Google **DNS** (on setup page)
6. [ ] [Ubuntu's search page](https://start.ubuntu.com/current/Google/?sourceid=hp) uses Google **Search**
7. [ ] [LineageOS](lineageos.org) uses Google **Analytics**; Google *CDNs*
8. [ ] [NetBSD](https://netbsd.org/) encourages the usage of Google's **DNS** in the Networking setup section of the installer.
9. [ ] [Kubuntu's homepage](https://kubuntu.org) uses Google *CDNs* (Google+ integration?)
10. [ ] [Fedora's website](https://getfedora.org) uses Google *Fonts*
11. [ ] [Elementary OS's website](https://elementary.io) uses Google *Fonts*
12. [ ] [Ubuntu's website](https://ubuntu.com) uses Google *Fonts*
13. [ ] [Pop_OS! website](https://pop.system76.com/) uses Google **Analytics**
14. [ ] [Lubuntu's website](https://lubuntu.net/) uses Google **Analytics**, Google *Fonts* and Google *CDNs*
15. [ ] [Ubuntu Budgie's website](https://ubuntubudgie.org) uses **Tag Manager**
16. [ ] [GNOME's website](https://www.gnome.org) uses Google *Fonts*
17. [ ] [GoboLinux's website](https://gobolinux.org) uses Google *Fonts*
18. [ ] [Void Linux's website](https://voidlinux.org) uses Google *Fonts*
19. [ ] [antiX Linux's website](https://antixlinux.com) uses Google **Translate**, *Fonts*, and **YouTube**
20. [ ] [MX Linux's website](https://mxlinux.org) uses Google **Translate**, *Fonts*, and **YouTube**
21. [ ] [KDE Neon's website](https://neon.kde.org) uses Google *Fonts*
22. [ ] [Solus' website](https://getsol.us/home/) uses Google *CDNs*
23. [ ] [FreeDOS' website](https://freedos.org) uses Google **Analytics**
24. [ ] [os-js' website](os-js.org) uses Google **Analytics** [see [issue#12](https://github.com/os-js/os-js.org/issues/12)]
25. [ ] [OpenMandriva's website](https://www.openmandriva.org) uses Google *CDNs*
26. [ ] [The AUR website](https://git.archlinux.org/aurweb.git/) uses Google *CDNs* [see [FS#61584](https://bugs.archlinux.org/task/61584)]

### Communication

1. [ ] [Riot's 'about' page](https://about.riot.im) uses Google *CDNs* and *Fonts*
2. [ ] [Matrix.org](https://matrix.org/) uses Google **reCAPTCHA** to create an account
3. [ ] [ProtonMail](https://protonmail.com) uses Google **reCAPTCHA**
4. [ ] [Signal's Support page](https://support.signal.org) uses Google **Analytics**; Google **reCAPTCHA**
5. [ ] [Rocket.Chat's website](https://rocket.chat) uses Google *Fonts*, **reCAPTCHA**, **Tag Manager** and **YouTube**
6. [ ] [Telegram's website](https://telegram.org/) uses Google **Analytics**
7. [ ] [Status.im website](https://status.im/) uses Google *Fonts*
8. [ ] [Jitsi's website](https://jitsi.org/) uses Google **Analytics**, Google *Fonts* and Google *CDNs*
9. [ ] [Lutris](https://lutris.net/) uses Google **Analytics**
10. [ ] [ZulipChat](https://zulipchat.com/) uses Google **Tag Manager**

### Development

1. [ ] [Webpack's website](https://webpack.js.org) uses Google **Analytics**; Google *CDNs* [see [pull#3759](https://github.com/webpack/webpack.js.org/pull/3759)]
2. [ ] [Preact's website](https://preactjs.com) uses Google **Analytics**
3. [ ] [Lodash's website](https://lodash.com) uses Google **Analytics**
4. [ ] [Goaccess.io](https://goaccess.io) uses Google *CDNs*
5. [ ] [IPFS' website](https://ipfs.io) uses Google **Analytics**
6. [ ] [Lazy.js' homepage](http://danieltao.com/lazy.js/) uses Google **Analytics**; Google *CDNs*
7. [ ] [QT's site](https://www.qt.io) uses Google *CDNs*; Google *Fonts*
8. [ ] [Godot's website](https://godotengine.org/) uses Google **Analytics**, Google *Fonts* and Google *CDNs*
9. [ ] [Vim's website](https://www.vim.org) uses Google **Search** and Google **Syndication**
10. [ ] [ohmyzsh's website](https://ohmyz.sh) uses **Tag Manager**
11. [ ] [Erlang's docs alternative](https://erldocs.com/) uses Google **Analytics**
12. [ ] [Erlang's docs](http://erlang.org/doc/man/) uses Google **Analytics**
13. [ ] [Blender](https://www.blender.org) uses Google **Analytics**
14. [ ] [Renpy](https://www.renpy.org/) uses Google **Analytics**
15. [ ] [openSSL](https://www.openssl.org/) uses Google *Fonts* and **CDN**
16. [ ] [Gitea's official instance](https://gitea.com) uses Google **Analytics**
17. [ ] [Neovim's website](https://neovim.io) uses Google *Fonts* and **Analytics**
18. [ ] [CDNJS](https://cdnjs.com) uses Google *Fonts* and **Analytics**
19. [ ] [Yeoman](https://yeoman.io) uses Google *Fonts* and **Analytics**
20. [ ] [Compiler Explorer](https://godbolt.org) uses Google **Analytics**
21. [ ] [Google Webfonts helper](https://google-webfonts-helper.herokuapp.com/) uses Google **Analytics**
22. [ ] [VueJS' website](https://vuejs.org) uses Google **Analytics**, *Fonts*
23. [ ] [D3JS' website](https://d3js.org) uses Google **Analytics**
24. [ ] [Animate Style's website](https://animate.style/) uses *Fonts*
25. [ ] [ThreeJS' website](https://threejs.org/) uses Google **Analytics**
26. [ ] [AngularJS' website](https://angularjs.org/) uses Google **Analytics**, *Fonts*
27. [ ] [RXJS' website](https://rxjs.dev/) uses Google **Analytics**, *Fonts*
28. [ ] [Material-UI's website](https://material-ui.com/) uses Google **Analytics**, *Fonts*
29. [ ] [ReduxJS' website](https://redux.js.org/) uses Google **Analytics**
30. [ ] [JQuery's website](https://jquery.com/) uses Google **Analytics**
31. [ ] [reveal.js' webiste](https://revealjs.com/) uses Google **Analytics**, *Fonts*
32. [ ] [Socket.IO's website](https://socket.io/) uses Google **Analytics**, *Fonts*, *CDNs*
33. [ ] [Chart.js' website](https://www.chartjs.org/) uses Google **Analytics**
34. [ ] [Semantic UI's website](https://semantic-ui.com/) uses Google **Analytics**, *Fonts*
35. [ ] [Element's (desktop UI library) Website](https://element.eleme.io/) uses Google **Analytics**
36. [ ] [Moment.js' website](https://momentjs.com/) uses Google **Analytics**, *Fonts*, *CDNs*
37. [ ] [ECHARTS' website](https://echarts.apache.org/en/index.html) uses **Tag Manager**
38. [ ] [Ionic's website](https://ionicframework.com/) uses **Tag Manager**, *Fonts*
39. [ ] [React Router's website](https://reacttraining.com/react-router/) uses Google **Analytics**, *YT*
40. [ ] [React Training's website](https://reacttraining.com/) uses Google **Analytics**, *Fonts*
41. [ ] [Material Design's website](https://material.io/) uses Google **Analytics**, **Tag Manager**, *Fonts*, *CDNs*
42. [ ] [Bulma's website](https://bulma.io/) uses Google **Analytics**, *CDNs*
43. [ ] [Normalize.css' website](https://necolas.github.io/normalize.css/) uses Google **Analytics**, *Fonts*
44. [ ] [Materialize's website](https://materializecss.com/) uses Google **Analytics**, *Fonts*
45. [ ] [Babel's website](https://babeljs.io/) uses Google **Analytics**
46. [ ] [Anime.js' website](https://animejs.com/) uses Google **Analytics**
47. [ ] [impress.js](https://impress.js.org/) uses Google **Analytics**, *Fonts*
48. [ ] [AdminLTE](https://adminlte.io/) uses Google **Analytics**
49. [ ] [Kotlin's website](https://kotlinlang.org/) uses Google **Tag Manager**, *Fonts*
50. [ ] [gulp.js' website](https://gulpjs.com/) uses Google **Analytics**, **Tag Manager**, *Fonts*
51. [ ] [mermaid-js' website](https://mermaid-js.github.io/mermaid/) uses *Fonts*, Google **Analytics**
52. [ ] [cdnjs' website](https://cdnjs.com/) uses Google **Analytics**, *Fonts*
53. [ ] [jQuery File Upload's webiste](https://blueimp.github.io/jQuery-File-Upload/) uses *CDNs*
54. [ ] [fullPage's website](https://alvarotrigo.com/fullPage/) uses **Tag Manager**
55. [ ] [Immutable's website](https://immutable-js.github.io/immutable-js/) uses *YT Image*
56. [ ] [Goodboy Digital's website](https://www.goodboydigital.com/) uses Google **Analytics**, *Fonts*
57. [ ] [Style Components' website](https://styled-components.com/) uses **Tag Manager**
58. [ ] [clipboard.js' website](https://clipboardjs.com/) uses Google **Analytics**, *Fonts*
59. [ ] [Foundation's (front-end framework) website](https://get.foundation/) uses *Fonts*
60. [ ] [Video JS' website](https://videojs.com/) uses Google **Analytics**, *Fonts*
61. [ ] [Leaflet's (JavaScript library) website](https://leafletjs.com/) uses Google **Analytics**, *Fonts*
62. [ ] [Day.js' website](https://day.js.org/) uses Google **Analytics**
63. [ ] [Phaser's (HTML5 game framework)website](https://phaser.io/) uses Google **Analytics**, **Tag Manager**
64. [ ] [Quill's (text editor) website](https://quilljs.com/) uses Google **Analytics**, CDN
65. [ ] [Async's website](https://caolan.github.io/async/v3/) uses *Fonts*
66. [ ] [Vuetify's website](https://vuetifyjs.com/en/) uses Google **Analytics**, *Fonts*
67. [ ] [Slick's (carousel) website](https://kenwheeler.github.io/slick/) uses Google **Analytics**
68. [ ] [Ant Design Pro's website](https://pro.ant.design/) uses Google **Analytics**
69. [ ] [Select2's (jQuery replacement for select boxes) website](https://select2.org/) uses Google **Analytics**, *Fonts*
70. [ ] [Modernizr's website](https://modernizr.com/) uses Google **Analytics**, *Fonts*
71. [ ] [tailwindcss' website](https://tailwindcss.com/) uses **Tag Manager**
72. [ ] [Ian Lunn's website](https://ianlunn.co.uk/) uses **Analytics**, **Tag Manager**
73. [ ] [View UI's website](https://www.iviewui.com/) uses Google **Analytics**
74. [ ] [Uppy's website](https://uppy.io/) uses Google **Analytics**, *CDNs*
75. [ ] [cheerio's (jQuery Implementation for server)website](https://cheerio.js.org/) uses *Fonts*
76. [ ] [webtorrent's website](https://webtorrent.io/) uses Google **Analytics**, *Fonts*
77. [ ] [MobX's website](https://mobx.js.org/README.html) uses Google **Analytics**
78. [ ] [particles.js' website](https://vincentgarreau.com/particles.js/) uses Google **Analytics**, *Fonts*, *CDNs*
79. [ ] [Tesseract.js' websute](https://tesseract.projectnaptha.com/) uses Google **Analytics**, *Fonts*
80. [ ] [Layui's website](https://www.layui.com/) uses Syndication
82. [ ] https://sheetjs.com/ uses Google **Analytics**
83. [ ] https://emberjs.com/ uses Google **Analytics**
84. [ ] https://hammerjs.github.io/ uses Google **Analytics**, *Fonts*
86. [ ] https://ricostacruz.com/nprogress/ uses Google **Analytics**, *Fonts*
87. [ ] https://airbnb.io/lottie/#/ uses GF,, Play, Image
88. [ ] https://purecss.io/ uses Google **Analytics**, *Fonts*
89. [ ] https://html2canvas.hertzen.com/ uses Google **Analytics**, *Fonts*
90. [ ] https://sweetalert.js.org/ uses *Fonts*
91. [ ] https://react-select.com/ uses *Fonts*, **Tag Manager**
92. [ ] https://photoswipe.com/ uses Google **Analytics**
93. [ ] https://fezvrasta.github.io/bootstrap-material-design/ uses Google **Analytics**, *Fonts*
94. [ ] https://redux.js.org/ uses Google **Analytics**
95. [ ] https://introjs.com/ uses Google **Analytics**, *CDNs*, *Fonts*
96. [ ] https://jade-lang.com/ uses *Fonts*
97. [ ] https://parall.ax/products/jspdf uses Google **Analytics**, **Tag Manager**
98. [ ] https://bluebirdjs.com/docs/getting-started.html uses Google **Analytics**, *Fonts*
99. [ ] https://scrollrevealjs.org/ uses Google **Analytics**
100. [ ] https://iris-go.com/ uses **Tag Manager**, *Fonts*, Syndication
101. [ ] https://draftjs.org/ uses Google **Analytics**
102. [ ] https://feathericons.com/ uses Google **Analytics**
103. [ ] https://swagger.io/ uses Google **Analytics**, *Fonts*, **Tag Manager**
104. [ ] https://popmotion.io/ uses Google **Analytics**, *Fonts*
105. [ ] https://getskeleton.com/ uses Google **Analytics**, *Fonts*, *CDNs*
106. [ ] https://www.react-spring.io/ uses *YT*
107. [ ] https://localforage.github.io/localForage/ uses *Fonts*
108. [ ] https://plyr.io/ uses **Tag Manager**, *CDNs*
109. [ ] https://tobiasahlin.com/spinkit/ uses *Fonts*
110. [ ] https://immerjs.github.io/immer/docs/introduction uses Google **Analytics**
111. [ ] https://facebook.github.io/flux/ uses *YT*
112. [ ] https://velocityjs.org/ uses Google **Analytics**, *Fonts*
113. [ ] https://highlightjs.org/ uses *Fonts*
114. [ ] https://howlerjs.com/ uses Google **Analytics**, *Fonts*
115. [ ] https://lesscss.org/ uses *Fonts*
116. [ ] https://blueprintjs.com/ uses Google **Analytics**
117. [ ] https://twitter.github.io/typeahead.js/ uses Google **Analytics**
118. [ ] https://fabricjs.com/ uses Google **Analytics**, *Fonts*
119. [ ] https://getuikit.com uses **Tag Manager**
120. [ ] https://mojs.github.io/ uses *Fonts*
121. [ ] https://mint-ui.github.io/ uses Google **Analytics**
122. [ ] https://nosir.github.io/cleave.js/ uses Google **Analytics**, *Fonts*
123. [ ] https://coffeescript.org/ uses Google **Analytics**, *Fonts*, *CDNs*
124. [ ] https://framework7.io/ uses Google **Analytics**
125. [ ] https://quasar.dev/ uses **Tag Manager**
126. [ ] https://www.dropzonejs.com/ uses Google **Analytics**, *Fonts*, *CDNs*
127. [ ] https://nostalgic-css.github.io/NES.css/ uses **Tag Manager**, *Fonts*
128. [ ] https://github.hubspot.com/pace/ uses Google **Analytics**, *CDNs*
129. [ ] https://documentup.com/kriskowal/q/ uses *Fonts*
130. [ ] https://zeptojs.com/ uses Google **Analytics**
131. [ ] https://masonry.desandro.com/ uses Google **Analytics**
132. [ ] http://matthew.wagerfield.com/parallax/ uses Google **Analytics**, *Fonts*
133. [ ] https://angular-ui.github.io/bootstrap/ uses Google **Analytics**, *Fonts*, *CDNs*
134. [ ] https://ionicons.com/ uses Google **Analytics**, *Fonts*
135. [ ] http://goratchet.com/ uses Google **Analytics**, *Fonts*, *CDNs*
136. [ ] https://shopify.github.io/draggable/ uses **Tag Manager**
137. [ ] http://riotjs.com/ uses *Fonts*
138. [ ] https://git-scm.com/ uses Google **Analytics**
139. [ ] [https://kubernetes.io/](https://github.com/kubernetes/website) uses Google **Analytics**, *Fonts*, **Tag Manager**
140. [ ] [PostgreSQL's website](https://github.com/postgres/pgweb) uses Google *Fonts*    
141. [ ] [erlang.org](https://github.com/erlang/erlang-org) uses Google *CDNs*, **Analytics**
142. [ ] [pypi.org](https://github.com/pypa/warehouse) uses Google **Tag Manager**, *Fonts*
143. [ ] [python.org](https://github.com/python/pythondotorg/blob/master/templates/base.html) uses Google **Analytics**, *CDNs*
144. [ ] https://rstudio.com/ uses Google **Tag Manager**, *Fonts*
145. [ ] https://www.metasploit.com/ uses Google **Tag Manager**, **YouTube**, *Fonts*
146. [ ] [cloudcv.org](https://github.com/Cloud-CV/CloudCV) uses Google **Analytics**, *Fonts*
147. [ ] [https://www.drupal.org/](https://git.drupalcode.org/project/drupalorg) uses Google **Analytics**, *Fonts*
148. [ ] [https://julialang.org/](https://github.com/JuliaLang/www.julialang.org) uses Google **Analytics**, *Fonts*, **YouTube**
149. [ ] https://www.postman.com/ uses Google **Analytics**, **Tag Manager**, *Fonts*
150. [ ] https://www.wireshark.org/ uses Google *Fonts*, *CDNs*, **Analytics**, **YouTube**
151. [ ] https://reactjs.org/ uses Google **Analytics**
152. [ ] https://reactnative.dev/ uses Google **Analytics**, **YouTube**
153. [ ] https://www.openstack.org/ uses Google **Analytics**, **Search (not sure)**
154. [ ] https://riscv.org/ uses Google **Search**, *Fonts*, **Tag Manager**, **CDN**
155. [ ] [mycroft.ai](https://mycroft.ai/) uses Google *Fonts* and **Tag Manager**
156. [ ] [opennmt.net](https://opennmt.net) uses Google *Fonts*, **Google Analytics** (see [issue](https://github.com/OpenNMT/opennmt.github.io/issues/30))

### News/Blogs/Education

1. [ ] [WordPress](https://wordpress.org) uses Google *Fonts*; Google *CDNs*
2. [ ] [ItsFOSS' website](https://itsfoss.com) uses Google **Analytics**
3. [ ] [LFS' home page](http://www.linuxfromscratch.org/lfs) uses Google **Search**
4. [ ] [Phoronix](https://www.phoronix.com/scan.php?page=home) uses Google **Analytics**; Google **Search**
5. [ ] [LinuxUprising](https://www.linuxuprising.com) uses Google **Analytics**; Google *CDNs*
6. [ ] [Tecmint](https://www.tecmint.com) uses Google **Search**
7. [ ] [UbuntuBuzz](https://www.ubuntubuzz.com/) uses Google **Analytics**; Google *CDNs*
8. [ ] [LWN.net](https://lwn.net) uses Google **Analytics**
9. [ ] [Openstax](https://openstax.org) uses Google *Fonts*
10. [ ] [FOSSdroid's website](https://github.com/SnaKKo/Fossdroid-Core) uses Google *Fonts*
11. [ ] [Rasberry Pi's website](https://www.raspberrypi.org/) uses Google **Tag Manager**, *Fonts*, **Ads**
12. [ ] [freeCodeCamp](https://www.freecodecamp.org/) uses Google **Analytics**, **Tag Manager**
13. [ ] [unpaywall](http://unpaywall.org/) uses Google **Analytics**, *Fonts*
14. [ ] https://github.com/github/opensource.guide (https://opensource.guide/) uses Google **Analytics**

### Others

1. [ ] [OnlyOffice](https://www.onlyoffice.com/) uses Google **Analytics**, *Fonts*, and **Tag Manager**
2. [ ] [Scribus](https://www.scribus.net/) uses Google **Fonts**
3. [ ] [Calibre](https://calibre-ebook.com/) uses Google **Analytics** and **Ads**
4. [ ] [Searx.be](https://searx.be) uses Google **Search**
5. [ ] [Island](https://github.com/oasisfeng/island) uses Google **Analytics**
6. [ ] [Exherbo's 'summer' search](https://git.exherbo.org/summer/) uses Google **Search**
7. [X] [XMonad](https://xmonad.org) uses Google **Search**
8. [ ] [Energized](https://energized.pro) uses Google *Fonts*
9. [ ] [StumpWM](https://github.com/stumpwm/stumpwm) uses Google **Search** (inside WM)
10. [X] [StumpWM's sample code](https://github.com/stumpwm/stumpwm/blob/fa77624ac0568228f8935926e1b355904b5f34ce/sample-stumpwmrc.lisp) promotes Google **Search**.
11. [ ] [OpenMW](https://openmw.org/en/) uses Google **Analytics** and **Youtube**
12. [ ] [ClamAV](https://www.clamav.net/) uses Google **Analytics**
13. [ ] [pfSense](https://www.pfsense.org/) uses Google **Analytics** and **Fonts**
14. [X] [iTerm's website](https://github.com/gnachman/iterm2-website) uses Google **Analytics**.
15. [ ] [Bitwarden's website](https://bitwarden.com/) uses Google **Analytics**, Google *Fonts*, Google *CDN*, Google **Tag Manager* and Google *Ajax*
16. [ ] [KISS Launcher's](https://kisslauncher.com/) uses Google **Tag Manager** and Google **Syndication**
17. [ ] [Flatpak](https://flatpak.org/) uses Google *CDN*, **YouTube**
18. [ ] [Pass's website](https://www.passwordstore.org) uses Google **Analytics**
19. [ ] [mtransit-for-android](https://github.com/mtransitapps/mtransit-for-android/wiki/Revenues) uses Google AdMob
20. [ ] [Nord color palette's website](https://www.nordtheme.com/) uses Google **Tag Manager**
21. [ ] [WineHQ's website](https://winehq.org) uses Google **Analytics**
22. [ ] [Hugo](https://gohugo.io/) uses Google **Tag Manager**
23. [ ] [VLC's website](https://code.videolan.org/VideoLAN.org/websites/-/tree/master/www.videolan.org) uses Google **Analytics**
24. [ ] [Krita's website](https://krita.org/en/) uses Google **Fonts** and **CDNs** 
25. [ ] [IndieAuth](https://indieauth.com) uses Google **Analytics**

### Jekyll themes / Personal Sites

1. [X] [aspiretheme/type](https://github.com/aspirethemes/type) uses Google **Analytics**
2. [X] [sujaykundu777/devlopr-jekyll](https://github.com/sujaykundu777/devlopr-jekyll) uses Google **Analytics**
3. [X] [christianezeani/panthera-jekyll](https://github.com/christianezeani/panthera-jekyll) uses Google **Analytics**
4. [X] [wu-kan/wu-kan.github.io](https://github.com/wu-kan/wu-kan.github.io) uses Google **Analytics**
5. [X] [linuxmint/linuxmint.github.io](https://github.com/linuxmint/linuxmint.github.io) uses Google **Analytics**
6. [ ] [Mario Ranftl's website](https://mranftl.com/) uses Google **Analytics**

## External links

1. [Comment by maskman1999](https://old.reddit.com/r/privacy/comments/gwsv5j/help_us_degoogle_foss_free_and_open_source/fsz3hpd/?context=3)
2. [Hyperbola](https://git.hyperbola.info:50100/software/blacklist.git/tree/blacklist.txt): containing a list of blocked packages; [original post](https://dev.lemmy.ml/post/34155/comment/6058). **DISCLAIMER: most are irrelevant and contain nonsensical reasoning. This is only kept due to relevancy of the project.**
